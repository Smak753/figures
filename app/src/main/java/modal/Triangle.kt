package modal
class Triangle(var a:Double, var b:Double, var c:Double): Figure(){
    override fun area(): Double {
        var p:Double=(a+b+c)/2
        result=Math.sqrt(p*(p-a)*(p-b)*(p-c))
        result = Math.floor(result * 100) / 100
        return result
    }

    override fun perimeter(): Double {
        result=a+b+c
        result = Math.floor(result * 100) / 100
        return result
    }
}