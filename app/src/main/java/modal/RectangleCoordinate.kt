package modal
class RectangleCoordinate(x1:Double,y1:Double, x2:Double,y2:Double):Figure(){
    var AOB =2*Math.sqrt(Math.pow(x2-x1,2.0)+Math.pow(y2-y1,2.0)) //нахождение длинны диагонали прямоугольника
    var a =Math.sqrt(Math.pow(AOB,2.0)/5) //нахождение меньшей из сторон прямоугольного треугольника через гипотенузу
    // считаем исходя из того, что b=2a в прямоугольном треугольнике
    override fun area(): Double {
        result= 2*Math.pow(a,2.0)
        result = Math.floor(result * 100) / 100
        return result
    }

    override fun perimeter(): Double {
    result=6*a
        result = Math.floor(result * 100) / 100
        return result
    }
}