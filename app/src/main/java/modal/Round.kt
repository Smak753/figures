package modal
class Round(var R:Double): Figure(){
    var pi =3.14
    override fun area(): Double {
        result = pi*Math.pow(R,2.0)
        result = Math.floor(result * 100) / 100
        return result
    }

    override fun perimeter(): Double {
        result=2*pi*R
        result = Math.floor(result * 100) / 100
        return result
    }
}