package com.example.classes

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import modal.Rectangle
import modal.RectangleCoordinate
import modal.Round
import modal.Triangle
import android.text.Spanned
import android.text.InputFilter
import kotlinx.android.synthetic.main.activity_area.*


class Area : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_area)
        var Figure_name =findViewById<TextView>(R.id.Figure)
        var a= findViewById<TextView>(R.id.a)
        var b= findViewById<TextView>(R.id.b)
        var c= findViewById<TextView>(R.id.c)
        var d= findViewById<TextView>(R.id.d)
        var a1= findViewById<TextView>(R.id.a1)
        var b1= findViewById<TextView>(R.id.b1)
        var c1= findViewById<TextView>(R.id.c1)
        var d1= findViewById<TextView>(R.id.d1)
        var Radius :Double
        var Width :Double
        var Hight :Double
        var A :Double
        var B :Double
        var C :Double
        var X :Double
        var Y :Double
        var Ox :Double
        var Oy :Double
        var Area =findViewById<TextView>(R.id.area_value)
        X = intent.getDoubleExtra("X",0.0)
        Y = intent.getDoubleExtra("Y",0.0)
        Radius = intent.getDoubleExtra("Radius",0.0)
        if(Radius!=0.0){
            Area.text = Round(Radius).area().toString()
            a.text =getText(R.string.Radius);a1.text=Radius.toString()
            Figure_name.text=getString(R.string.Round)
        }

        Width= intent.getDoubleExtra("Width",0.0)
        Hight= intent.getDoubleExtra("Hight",0.0)
        if(Width!=0.0||Hight!=0.0){Area.text = Rectangle(Width,Hight).area().toString()
            a.text =getText(R.string.Width);a1.text=Width.toString()
            b.text =getText(R.string.Hight);b1.text=Hight.toString()
            Figure_name.text=getString(R.string.Rectangle)
        }

        A = intent.getDoubleExtra("A",0.0)
        B = intent.getDoubleExtra("B",0.0)
        C = intent.getDoubleExtra("C",0.0)
        if(A!=0.0||B!=0.0||C!=0.0){Area.text = Triangle(A,B,C).area().toString()
            a.text =getText(R.string.Side_a);a1.text=A.toString()
            b.text =getText(R.string.Side_b);b1.text=B.toString()
            c.text =getText(R.string.Side_c);c1.text=C.toString()
            Figure_name.text=getString(R.string.Triangle)
        }

        Ox = intent.getDoubleExtra("Ox",0.0)
        Oy = intent.getDoubleExtra("Oy",0.0)
        if(Ox!=0.0||Oy!=0.0){Area.text = RectangleCoordinate(Ox,Oy,X,Y).area().toString()
            a.text =getText(R.string.Input_x);a1.text=Ox.toString()
            b.text =getText(R.string.Input_y);b1.text=Oy.toString()
            c.text =getText(R.string.Center_x);c1.text=X.toString()
            d.text =getText(R.string.Center_y);d1.text=Y.toString()
            Figure_name.text=getString(R.string.Rectangle)
        }

    }
}
