package com.example.classes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Spinner
import android.content.Intent
import android.text.InputFilter
import android.text.Spanned
import android.widget.EditText


class MainActivity : AppCompatActivity() {
    inline fun String.toDouble(): Double = java.lang.Double.parseDouble(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var Spinner = findViewById<Spinner>(R.id.spinner)
        var X = findViewById<EditText>(R.id.X)
        var Y = findViewById<EditText>(R.id.Y)
        var Btn = findViewById<Button>(R.id.Start)
        var intent:Intent
        Btn.setOnClickListener(View.OnClickListener {
var Selected = Spinner.selectedItemPosition
            when(Selected) {
                0 -> {
                    intent = Intent(applicationContext, com.example.classes.Round::class.java)
                }
                1 -> {
                    intent = Intent(applicationContext, com.example.classes.Rectangle::class.java)
                }
                2 -> {
                    intent = Intent(applicationContext, com.example.classes.RectangleCoordinate::class.java)
                }
                3 -> {
                    intent = Intent(applicationContext, com.example.classes.Triangle::class.java)
                }
                else -> {
                    intent = Intent(applicationContext, MainActivity::class.java)
                }
            }
            intent.putExtra("X", X.text.toString().toDouble())
            intent.putExtra("Y", Y.text.toString().toDouble())
            startActivity(intent)
        })
        X.setFilters(arrayOf(filter))
        Y.setFilters(arrayOf(filter))
        }
    var filter: InputFilter = object : InputFilter {
        internal val maxDigitsBeforeDecimalPoint = 4
        internal val maxDigitsAfterDecimalPoint = 3

        override fun filter(source: CharSequence, start: Int, end: Int,
                            dest: Spanned, dstart: Int, dend: Int): CharSequence? {
            val builder = StringBuilder(dest)
            builder.replace(dstart, dend, source
                    .subSequence(start, end).toString())
            return if (!builder.toString().matches(("-?[0-9]{0," + (maxDigitsBeforeDecimalPoint-1) + "}+((\\.[0-9]{0," + (maxDigitsAfterDecimalPoint-1) + "})?)||(\\.)?").toRegex())) {
                if (source.length == 0) dest.subSequence(dstart, dend) else ""
            } else null

        }
    }
    }
