package com.example.classes

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import android.view.View
import android.widget.*

class Rectangle : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rectangle)
        var X =findViewById<TextView>(R.id.X)
        var Y =findViewById<TextView>(R.id.Y)
        var Width = findViewById<EditText>(R.id.width)
        var Hight = findViewById<EditText>(R.id.hight)
        var Spinner = findViewById<Spinner>(R.id.spinner)
        var Btn = findViewById<Button>(R.id.Calculate)
        X.text = intent.getDoubleExtra("X",0.0).toString()
        Y.text = intent.getDoubleExtra("Y",0.0).toString()
        Btn.setOnClickListener(View.OnClickListener {
            if(Width.text.toString()==""||Hight.text.toString()==""||Width.text.toString().toDouble()==0.0||Hight.text.toString().toDouble()==0.0)
            { Toast.makeText(this,"Not found value", Toast.LENGTH_SHORT).show()}
            else {
                var Selected = Spinner.selectedItemPosition
                when (Selected) {
                    0 -> {
                        intent = Intent(applicationContext, com.example.classes.Area::class.java)
                    }
                    1 -> {
                        intent = Intent(applicationContext, com.example.classes.Perimeter::class.java)
                    }
                    else -> {
                        intent = Intent(applicationContext, com.example.classes.Rectangle::class.java)
                    }
                }
                intent.putExtra("Width", Width.text.toString().toDouble())
                intent.putExtra("Hight", Hight.text.toString().toDouble())
                startActivity(intent)
            }})
        Width.setFilters(arrayOf(filter))
        Hight.setFilters(arrayOf(filter))
    }
    var filter: InputFilter = object : InputFilter {
        internal val maxDigitsBeforeDecimalPoint = 4
        internal val maxDigitsAfterDecimalPoint = 3

        override fun filter(source: CharSequence, start: Int, end: Int,
                            dest: Spanned, dstart: Int, dend: Int): CharSequence? {
            val builder = StringBuilder(dest)
            builder.replace(dstart, dend, source
                    .subSequence(start, end).toString())
            return if (!builder.toString().matches(("[0-9]{0," + (maxDigitsBeforeDecimalPoint-1) + "}+((\\.[0-9]{0," + (maxDigitsAfterDecimalPoint-1) + "})?)||(\\.)?").toRegex())) {
                if (source.length == 0) dest.subSequence(dstart, dend) else ""
            } else null

        }
    }
}
